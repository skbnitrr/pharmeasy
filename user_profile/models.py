from django.db import models
from django.contrib.auth.models import AbstractUser
from model_utils.models import TimeStampedModel


class User(AbstractUser, TimeStampedModel):
    USER_TYPE = (
        ('PATIENT', 'Patient'),
        ('DOCTOR', 'Doctor'),
        ('PHARMACIST', 'Pharmacist')
    )

    user_type = models.CharField(max_length=20, choices=USER_TYPE)

    class Meta:
        db_table = 'user'
        app_label = 'user_profile'
