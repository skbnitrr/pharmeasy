Execute following commands to run application

pip install -r requirements.txt

python manage.py migrate


API Doc

List of Prescription Requests - /api/prescription/request
Method: GET
Request Header: Authorization Token

Create Prescription Request - /api/prescription/request/
Method: POST
Request Header: Authorization Token, Content-Type application/x-www-form-urlencoded
Form Data: patient <patient_id>

Update Prescription Request Status - /api/prescription/<request_id>/
Method: PUT
Request Header: Authorization Token, Content-Type application/x-www-form-urlencoded
Form Data: status <APPROVED/REJECTED>

Retrieve Prescription - /api/prescription/<request_id>/retrieve/
Method: GET
Request Header: Authorization Token