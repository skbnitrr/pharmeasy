from django.conf.urls import url
from views import PrescriptionRequestApiView, PrescriptionRetrieveView, PrescriptionRequestUpdateView

urlpatterns = [
    url(
        regex=r'^request/$',
        view=PrescriptionRequestApiView.as_view(),
        name="prescription-request"
    ),
    url(
        regex=r'^(?P<pk>.+)/retrieve/$',
        view=PrescriptionRetrieveView.as_view(),
        name="view-prescription"
    ),
    url(
        regex=r'^(?P<pk>.+)/$',
        view=PrescriptionRequestUpdateView.as_view(),
        name="prescription-request-update"
    )
]