from django.db import models
from model_utils.models import TimeStampedModel

# Create your models here.
from user_profile.models import User


class PrescriptionRequest(TimeStampedModel):

    STATUS = (
        ('PENDING', 'Pending'),
        ('APPROVED', 'Approved'),
        ('REJECTED', 'Rejected')
    )

    patient = models.ForeignKey(User, related_name='patient_user')
    requested_by = models.ForeignKey(User, related_name='requested_by')
    status = models.CharField(max_length=10, choices=STATUS, default='PENDING')

    class Meta:
        db_table = 'prescription_request'


class PatientPrescription(TimeStampedModel):
    patient = models.ForeignKey(User, related_name='patient_prescription')
    prescription_url = models.URLField()

    class Meta:
        db_table = 'patient_prescription'
