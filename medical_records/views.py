from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from medical_records.models import PrescriptionRequest, PatientPrescription
from medical_records.permissions import PrescriptionRequestPermission, PrescriptionApprovePermission


class PrescriptionRequestApiView(ListCreateAPIView):

    permission_classes = (IsAuthenticated, PrescriptionRequestPermission)

    def get_queryset(self):
        return PrescriptionRequest.objects.filter(requested_by=self.request.user)

    def post(self, request, *args, **kwargs):
        try:
            prescription_request = PrescriptionRequest(requested_by=request.user, patient=request.POST['patient'])
            prescription_request.save()
            return Response(data={'message': 'Request Created successfully'}, status=status.HTTP_201_CREATED)
        except KeyError:
            return Response(data={'error': 'Patient Id is required'}, status=status.HTTP_400_BAD_REQUEST)


class PrescriptionRequestUpdateView(UpdateAPIView):

    queryset = PrescriptionRequest.objects.all()
    permission_classes = (IsAuthenticated, PrescriptionApprovePermission)
    lookup_field = 'pk'
    lookup_url_kwarg = 'pk'

    def put(self, request, *args, **kwargs):
        request_status = request.POST['status']
        prescription_request = PrescriptionRequest.objects.get(pk=kwargs['pk'])
        prescription_request.status = request_status
        prescription_request.save()
        return Response(data={'message': 'Request Updated Successfully'})


class PrescriptionRetrieveView(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            request_id = self.kwargs['request_id']
            prescription_request = PrescriptionRequest.objects.get(pk=request_id, requested_by=request.user)

            if prescription_request.status == 'APPROVED':
                prescription = PatientPrescription.objects.get(patient=prescription_request.patient)
                return Response(data={'prescription_url': prescription.prescription_url})
            return Response(data={'error': 'User not authorized to view prescription'}, status=status.HTTP_403_FORBIDDEN)
        except ObjectDoesNotExist:
            return Response(data={'error': 'Request does not exist'})
