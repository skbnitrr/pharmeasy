from rest_framework.permissions import BasePermission

from auth.models import User


class PrescriptionRequestPermission(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated() and request.user.user_type in ['DOCTOR', 'PHARMACIST']:
            return True
        return False


class PrescriptionApprovePermission(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated() and request.user.user_type == 'PATIENT':
            return True
        return False
